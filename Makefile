DEVICE := nut

MODEM_IMAGE := firmware-update/NON-HLOS.bin
VERSION := $(shell strings $(MODEM_IMAGE) | sed -n 's|QC_IMAGE_VERSION_STRING\=MPSS\.DPM\.\(.*\)|\1|p')

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update
	zip -r9 $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Inspect
# ==========

.PHONY: inspect
inspect: $(MODEM_IMAGE)
	@echo Target: $(TARGET)
	@echo Version: $(VERSION)
